@extends('adminLTE.master')

@section('judul')
    Daftar berhasil
@endsection

@section('content')
    <div class="px-3">
        <h1>Selamat Datang {{ $firstname }} {{ $lastname }}</h1>
        <h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
    </div>
@endsection
